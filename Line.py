import cv2
import numpy as np
from collections import deque

class line():
    def __init__(self):
        self.detected = False
        # x values of the last n fits of the line
        self.recent_xfitted = deque([], 500)
        # y values of the last n fits of the line
        self.recent_yfitted = deque([], 500)
        #polynomial coefficients for the most recent fit
        self.current_fit = [np.array([False])]
        #radius of curvature of the line in some units
        self.radius_of_curvature = None
        #distance in meters of vehicle center from the line
        self.line_base_pos = None
        #x values for detected line pixels
        self.allx = None
        #y values for detected line pixels
        self.ally = None
        # smooth factor
        self.smooth_factor = 15

        # Maintain a list of recent curve radii
        self.recent_curveradii = deque(100*[0], 100)

        # Minimum pixels required for line detection
        self.pixel_threshold = 50 # at least 50 pixels required to detect the line


    def fit(self, x,y):
        self.allx  = x
        self.ally = y

        # Fit a second order polynomial
        if len(x) > self.pixel_threshold:
            #print("Pixels found. Fitting polynomial...")
            self.current_fit = np.polyfit(y, x, 2)
            #print("polynomial coefficients:",self.current_fit)
            self.detected = True
        else:
            #print("Pixels NOT found. Fitting over last 500")
            self.detected = False
            # Otherwise, fit a line over last 500 values of x and y
            self.current_fit = np.polyfit(list(self.recent_yfitted), list(self.recent_xfitted), 2)
        return self.current_fit

    def get_curverad(self, img_height,xm_per_pix, ym_per_pix):
        ploty = np.linspace(0, img_height-1, num=img_height)# to cover same y-range as image
        y_eval = np.max(ploty)
        fit_cr = np.polyfit(ploty*ym_per_pix, self.fitx*xm_per_pix, 2)
        curverad = ((1 + (2*fit_cr[0]*y_eval*ym_per_pix + fit_cr[1])**2)**1.5) / np.absolute(2*fit_cr[0])
        self.radius_of_curvature = curverad
        return self.radius_of_curvature

    def get_fitted_curve_pixel_positions(self, img_shape, fit):
        """
        Accept image and indices of non zero pixels and return curved
        lane lines, as pixel positions
        """
        ploty = np.linspace(0, img_shape[0]-1, num=img_shape[0])
        #left_fit and right_fit are the pixel co-ordinates
        # Here we're generating pixel co-ordinates for the curved line
        self.fitx = fit[0]*ploty**2 + fit[1]*ploty + fit[2]

        return self.fitx
