## Advanced Lane Finding Project
Project report by: Swapnil Devikar

---

## Goals
The goals / steps of this project are the following:

* Compute the camera calibration matrix and distortion coefficients given a set of chessboard images.
* Apply a distortion correction to raw images.
* Use color transforms, gradients, etc., to create a thresholded binary image.
* Apply a perspective transform to rectify binary image ("birds-eye view").
* Detect lane pixels and fit to find the lane boundary.
* Determine the curvature of the lane and vehicle position with respect to center.
* Warp the detected lane boundaries back onto the original image.
* Output visual display of the lane boundaries and numerical estimation of lane curvature and vehicle position.

## Project Contents:
* P4.ipynb: The main notebook containing code and output images at each step
* camera_calibration.ipynb: A separate notebook that performs camera calibration and saves the calibration output to a specified folder 'camera_cal'
* camra_cal: A folder containing the saved camera calibration
* Line.py: A separate supporting module that provides fitted curve tracking APIs
* lane_marked_output.mp4: Output video produced by P4.ipynb

[//]: # (Image References)

[image1]: ./output_images/undistorted.png "Undistorted"
[image2]: ./output_images/thresholded.png "Binary Example"
[image3]: ./output_images/perspective_transformed.png "Warp Example"
[image4]: ./output_images/lane_fitted.png "Fit Visual"
[image5]: ./output_images/projected_lane.png   "Sample Output"
[video1]: ./lane_marked_output.mp4 "Video"

## [Rubric](https://review.udacity.com/#!/rubrics/571/view) Points
### Here I will consider the rubric points individually and describe how I addressed each point in my implementation.

---

### Writeup / README

####1. Provide a Writeup / README that includes all the rubric points and how you addressed each one.
You're reading it!

### Camera Calibration

Camera calibration is performed seperately in ./camera_calibration.ipynb notebook. The reason for choosing a different notebook for this process was to avoid calibrating camera multiple times in main notebook (./P4.ipynb) , as "Run All" cells operation was expected to be performed multiple times.
In[5] of camera_calibration.ipynb provides two helper functions:
- find_corners: Returns objpoints and imgpoints for a given list of images with given x and y corners and channels. This is done for all successful detections
- perform_camera_calibration: Uses the objpoints and imgpoints above to perform camera calibration

In[6] calls the perform_camera_calibration function and saves the calibration parameters in pickle format.

### Pipeline (single images)

#### 1. An example of a distortion-corrected image.
To demonstrate this step, I will describe how I apply the distortion correction to one of the test images like this one:
![alt text][image1]

The above image is taken directly from the 'P4.ipynb' notebook, for 'test_images/test1.jpg' as input. As it is seen in the result, there's not much of a difference between the original and the undistorted image. Although, the difference is noticable at the corners, which confirms that the distortion correction is working properly.
The same process was repeated for different images (manually) and the result was visually inspected.

#### 2. Performing color transforms, gradients to create a thresholded binary image.
I used a combination of color and gradient thresholds to generate a binary image (thresholding steps are in cells In[4] `P4.ipynb`).  Here's an example of my output for this step.
![alt text][image2]
I experimented with several combinations to get a reasonable output in order to avoid accounting for imperfections and artifacts in the sliding window phase. Several helper functions were defined to enable this. Some of those helper methods are as follows:
- get_s_channel_hsv
- get_l_channel_hls
- get_scaled_sobelx
- get_thresholded_x_gradient_binary
- get_color_channel_thresholded_binary
- get_overall_combined_binary

Finally apply_color_and_gradient_threshold function makes use of some of these methods and drives the main combination logic to get the desired output above.

#### 3.Perspective transform and provide an example of a transformed image.

The code for my perspective transform is present in sell In[5] cell of 'P4.ipynb' notebook. This cell defines some helper functions again to perform perspective transform. Taking an idea from the Q/A for P4, I implemented a slightly modified version that suited my needs. Here's the functions that I implemented to enable this step:
- get_perspective_transform_locations: Takes image shape as an input and returns the source and destination points for warping, using some pre-defined ratios.

```
bot_width = .76 # multiplication factor for the bottom width of source trapezoid
mid_width = .08 # multiplication factor for the top width of source trapezoid
height_pct = .62 # multiplication factor for the trapezoid height
bottom_trim = .935 # this much percent will be the offset from the bottom to avoid car hood
```
As a result of the above ratios, I get the source and destination points as follows:

This resulted in the following source and destination points:

| Source        | Destination   |
|:-------------:|:-------------:|
| 588, 446      | 180, 0        |
| 691, 446      | 1100, 0       |
| 153, 673      | 180, 720      |
| 1126, 673     | 1100, 720     |

The above points are marked with red dots on input and perspective transformed image respectively. A side by side comparison is shown below

![alt text][image3]

#### 4. Identifying lane-line pixels and fit their positions with a polynomial
I implmented the histogram and sliding window approach to identify the lane lines and subsequently fit a second order polynomial through the identified pixels.
Some of these methods are follows. These are present in the In[6] celll in 'P4.ipynb' notebook.
- get_lane_positions: Accepts hitogram as an input and returns the corresponding x co-ordinates for lane positions
- stack_sliding_windows: Implements the sliding window algorithm to identify the pixels corresponding to lane lines and returns a list of these pixel positions

I defined a separate class called 'Line.py' to keep track of these pixel positions and fit a polynomial through these points. Left and right lane, each create an object of the Line class and uses it to provide APIs that provide information about the fitted line, such as curve radius.
Also, Line class implements some basic error correction mechanism, e.g. what to do when no lane lines are identified in a frame.

The result of fitted polynomial for one of the images is shown below:

![alt text][image4]

#### 5. Calculating the radius of curvature of the lane and the position of the vehicle with respect to center.
The APIs for these is implemented by Line class. Here's how you can get the curve radius for a lane
```
curverad = lineobj.get_curverad(binary_warped.shape[0],xm_per_pix, ym_per_pix)
```
Calculating the vehicle position is done using the helper function get_vehicle_position as below:
```
center_diff, side_pos = get_vehicle_position(left_fitx, right_fitx)
```
An example of this can be found in In[6] cell in 'P4.ipynb' notebook.

#### 6. Projecting lane area in original image

This part is implemented in the cells In[8] using 'project_lanes_on_original_image' helper function. This function takes in the binary warped and undistorted image with camera calibration parameters and fitted polynomial coefficients and projects the lane lines with a colored lane area polynomial on unwarped image.

I also wrote write_radii_center_pos() function to write the curve radius and vehicle position values on the output image.
The final result is shown below:

![alt text][image5]

---

### Pipeline (video)

#### 1. The link to the output video is below

Here's a [link to my video result](./lane_marked_output.mp4)

---

### Discussion

#### 1. Issues faced:
This was by far the toughest project in the carnd in my opinion. Although much of the code samples were readily available, there was a lot of pieces to put together in order to get a working pipeline, especially for an inexperienced python coder.
I did struggle a little bit with the source and destination transform points. Because the lane start and end positions along the vertical axis was different for different test images. As a result, coming up with a reasonable compromise was an iterative process. Finally, I turned to the Q/A to get some ideas.
My second roadbloack was to come up with just a right combination of the color and gradient thresholding to produce a good output. I wanted to do that because, I was not planning to implement a fancy sliding window technique, to save the coding and debugging effort, especially with the limited time I had at my disposal.
The third and the biggest roadblock was making use of previously fitted line information to fit new lines, as the class material had hinted. This became particularly tricky while handling the video. There were too many parameters to tune. e.g. how far back should I look into the history, how do I save the data without running out of memory and some other coding related issues. I finally implemented the Line class and performed the bare minimum error correction mechanism in it. However, I did not implement anything to check the 'sanity' or curve radius or the fitted polynomial coefficients. The coding part of this error correction would have been not much of work, however, doing it correctly so that, for example, avoiding inclusion of bad data so that correct data is not skewed would have been tricky. Rejecting bad data, for example, bad polynomial coefficients was also tricky because, we don't know in advance, what the "correct" polynomial coefficients should be. I am planning to revisit this part after the end of the term and explore how this can be done reliably.

#### 2. Current limitations:
- Sometimes the lines are slightly wobbly. Specially, the top right corner. This is because of the fitted polynomial tries the skew the right lane towards right. The result of cell In[6] provides a good case for why this might be happening. This artifact, however, is not at all visible, unless we look at the actual video.
- Curveradius sometimes evaluates to a value that's quite different (e.g. 3 times) the previous one. This also happens because in some frames, lane lines are not clear and the fitted polynomials for left and right lanes tend to skew in opposite directions. This problem is also quite fixable by keeping a record of previous n curve radii and taking the odd man out. However, I did not implement this because of the reasons mentioned in the Issues faced section above. However, as it is observed from the video, the curve radius changes as per expectations. e.g. for a sharp turn, the curve radius is smaller and for near straight lane lines, the curve radius is bigger. In a lot of frames, curve radius is reported as ~1000 meters, which is the expected value.
