
import numpy as np
from collections import deque

class tracker():
    def __init__(self, nrecords = 10):
        # number of records to keep
        self.nrecords = nrecords
        # recent left lane polyfit coefficients
        self.recent_left_polyfits = deque([], nrecords)
        # recent right lane polyfit coefficients
        self.recent_right_polyfits = deque([], nrecords)
        # best fit for left lane
        self.best_left = []
        # best fit for right lane
        self.best_right = []
        # count of how many polyfits have been tracked so far
        self.count = 0
        # keep track of where the lane lines start
        self.recent_leftx_bases = deque([], nrecords)
        # keep track of where the lane right start
        self.recent_rightx_bases = deque([], nrecords)

    def is_consistent(self, leftx_base, rightx_base):
        #print("Received bases: leftx_base:{}, rightx_base:{}".format(leftx_base, rightx_base))
        res = True
        self.count = np.clip((self.count + 1), 0, self.nrecords)
        self.current_lane_width = rightx_base - leftx_base
        #print("Current lane width: ", self.current_lane_width)
        if(self.count >= self.nrecords):
            # calculate the average lane width so far
            avg_lane_width = np.mean(self.recent_rightx_bases) - np.mean(self.recent_leftx_bases)
            #print("Average lane width: ", avg_lane_width)
            # then see if current lane width makes sense
            res = abs(self.current_lane_width - avg_lane_width) < 150
        else:
            res = (self.current_lane_width >= 650 and self.current_lane_width <= 850)

        #print("is_consistent returned ", res)
        return res

    def get_best_fit(self):
        #print("Getting the best fit...")
        return self.best_left, self.best_right

    def track(self, leftfit, rightfit, leftx_base, rightx_base):
        # check if leftx and rightx are consistent and if yes, save them in recents
        #print("tracking..",leftfit, rightfit)
        self.recent_left_polyfits.append(leftfit)
        self.recent_right_polyfits.append(rightfit)
        self.recent_leftx_bases.append(leftx_base)
        self.recent_rightx_bases.append(rightx_base)
        if self.count >= self.nrecords:
            #print("self.recent_left_polyfits:{}", self.recent_left_polyfits)
            #print("self.recent_right_polyfits:{}", self.recent_left_polyfits)
            #print("best_left:", np.average(self.recent_left_polyfits, axis=0))
            #print("best_right:", np.average(self.recent_right_polyfits, axis=0))
            self.best_left = np.average(self.recent_left_polyfits, axis=0)
            self.best_right = np.average(self.recent_right_polyfits, axis=0)
        else:
            self.best_left = self.recent_left_polyfits[-1]
            self.best_right = self.recent_right_polyfits[-1]
